import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "../home/components/home/home.component";
import {NgModule} from "@angular/core";
import {ShopComponent} from "./components/shop/shop.component";

const routes: Routes = [
  { path: '', component: ShopComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
