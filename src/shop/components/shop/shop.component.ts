import { Component, OnInit } from '@angular/core';
import {HttpService, IItem} from "../../../core/http.service";
import {ActivatedRoute, Router} from "@angular/router";
import {filter, withLatestFrom} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  public items: IItem[] = [];

  constructor(
    private http: HttpService,
    private router: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.http.list<IItem>()
      .pipe(
        withLatestFrom(
          this.router.queryParams
        )
      )
      .subscribe((data) => {
        const [items, params] = data;
        this.items = items.filter(item => !params.brand || item.brand === params.brand);
      });
  }

}
