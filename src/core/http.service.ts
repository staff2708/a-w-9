import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

const enum color {
  RED = 'red',
  GREEN = 'green',
  BLUE = 'blue',
  BLACK = 'black',
  WHITE = 'white',
}

export const enum brand {
  PANAMA_JACK = 'Panama Jack',
  EPAM = 'Epam',
  GLOBAL_LOGIC = 'Global Logic',
  INFOPULSE = 'InfoPulse',
}

export interface IItem {
  id: string,
  sale: boolean,
  color: color,
  price: number,
  brand: brand,
  name: string,
  picture: string,
  rating: number
}

@Injectable()
export class HttpService {

  private headers = {
    'Content-Type': 'application/json',
  };

  private url = 'https://angular-workshop-server.herokuapp.com/api/item';

  constructor(
    private http: HttpClient
  ) { }

  public list<T>(): Observable<T[]> {
    return this.http.get<T[]>(this.url, {
      headers: this.headers
    })
  }
}
