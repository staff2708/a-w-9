import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public brands = [
    'Panama Jack',
    'Epam',
    'Global Logic',
    'InfoPulse'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
